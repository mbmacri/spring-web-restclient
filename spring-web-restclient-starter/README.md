# Spring Dynamic REST Client

Este _Starter_ de Spring Boot permite implementar fácilmente clientes REST utilizando la misma notación (annotations) utilizada para definir REST Controlers mediante SpringMVC.

La dependencia de MAVEN, mediante Jitpack, es la siguiente:

```xml
		<dependency>
			<groupId>com.wenance</groupId>
			<artifactId>spring-web-restclient</artifactId>
			<version>2.0.1.8-SNAPSHOT</version>
		</dependency
```
Para habilitar esta extensión simplemente se debe agregar el annotation `@EnableRestClients` en una clase de configuración de Spring Boot, indicando el nombre del package donde vas a colocar las interfaces de los clientes y, opcionalmente, un endpoint por defecto. 

Este es un ejemplo:

```java
@Configuration
@EnableRestClients(classPaths = "com.wenance.sorisky.riskplugins.clients", defaultEndPoint = "http://localhost:8080")
@ComponentScan("com.wenance.sorisky.riskplugins.plugins")
public class SoriskyEngineRiskPluginsConfiguration {
}
```

Después, por cada servicio que se desea consumir (donde “servicio” es una URL base, que puede tener varios endpoints), se debe definir una interfaz con el annotation `@RestMapping`. Luego, dentro de la interfaz se debe anotar cada método utilizando los mismos annotations que en SpringMVC, 

```java
@RestMapping
@RequestMapping(method = RequestMethod.POST, path = "/connectors/api/v1", consumes = "application/json", produces = "application/json")
public interface BureauConnectorClient {

	@RequestMapping(method = RequestMethod.POST, path="/{bureau}/{model}/{version}")
	public Map<String, Object> callBureau(@PathVariable(name = "bureau", required = true) String bureau, //
			@PathVariable(name = "model", required = true) String model, //
			@PathVariable(name = "version", required = true) String version, //
			@RequestBody Map<String, Object> requestParameters);

	@RequestMapping(method = RequestMethod.GET, path="/bureaus/{id}")
	public Bureau getBureauInfo(@PathVariable(name = "id", required = true) String id);

}
```
Durante el inicio, la librería 

Durante la inicialización de la aplicación, la librería busca en el package especificado todas las interfaces anotadas como `@RestMapping`, crea un _Dynamic REST Client_ por cada una (que implementa las llamadas REST declaradas en la interfaz) y los registra como Beans en el ApplicationContext de Spring.

Por último, para utilizar un Dynamic RestClient, simplemente hay que hacer un `@Autowired` de la interfaz definida:

```java
@Component
public class BureauConsumerPlugin implements ExecutionPlugin {

	@Autowired
	private BureauConnectorClient client;
	
	...
}
```
Para poder especificar la URL base de cada servicio, se pueden utilizar properties. Para eso, se define una propert con el sufijo “rest.client” seguido del qualified name de la interfaz. El _Dynamic REST Client_ se encarga de componer automáticamente las URLs y mapear los objetos a/desde JSON tanto en los parámetros como en la respuesta.

```properties
rest.client.endpoint.com.wenance.sorisky.riskplugins.plugins.BureauConsumerPlugin=http://192.167.1.89:345/gateway
```

