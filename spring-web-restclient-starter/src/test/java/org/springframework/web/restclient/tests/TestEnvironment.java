package org.springframework.web.restclient.tests;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.restclient.DynamicRestClientProperties;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, //
		properties = { "server.port=8088", //
				"server.servlet.context-path=/test", //
				"logging.level.org.springframework.web.restclient.core=TRACE"
				})

@ContextConfiguration(classes = TestServiceConfig.class)
public class TestEnvironment {

	@Autowired
	DynamicRestClientProperties props;
	
	@Test
	public void testEnvironmentHashmap () {
		assertThat(props).isNotNull();
		
		
		assertThat(props.getDefaultEndpoint()).isEqualTo("coco");
		assertThat(props.getEndpoint().get("foo.var.boo")).isEqualTo("pepa");
		assertThat(props.getEndpoint().get("foo.var.zoo")).isEqualTo("coco");
	}
}
