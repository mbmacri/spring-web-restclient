package org.springframework.web.restclient.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

public class BadRequestHttpRestException extends HttpRestException {
	private static final long serialVersionUID = -5686587637134399751L;

	public BadRequestHttpRestException() {
		super();
	}

	public BadRequestHttpRestException(ClientHttpResponse response) {
		super(response);
	}

}
