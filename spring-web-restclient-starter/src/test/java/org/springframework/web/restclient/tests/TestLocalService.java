package org.springframework.web.restclient.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, //
		properties = {"server.port=8088",//
				"server.servlet.context-path=/test", //
				"logging.level.org.springframework.web.restclient.core=TRACE"})
		
@ContextConfiguration(classes = TestServiceConfig.class)
public class TestLocalService {

	@Autowired
	TestService service;

	@Autowired
	TestServiceImpl serviceImpl;

	@Test
	public void testValue() throws InterruptedException {
		service.headerTest("This is my value");

		Thread.sleep(100);

		assertEquals("This is my value", serviceImpl.receivedArg);
	}
	@Test
	public void testList() throws InterruptedException {
		List<String> arg = new ArrayList<>();
		
		arg.add("value1");
		arg.add("value2");
		arg.add("value3");
		
		
		service.headerTest(arg);
		
		Thread.sleep(100);
		
		assertThat(serviceImpl.receivedList).isEqualTo(arg);
	}
}
