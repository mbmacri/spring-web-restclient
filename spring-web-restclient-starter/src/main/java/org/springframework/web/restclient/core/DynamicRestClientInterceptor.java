package org.springframework.web.restclient.core;

import static java.util.stream.Collectors.joining;

import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DynamicRestClientInterceptor implements MethodInterceptor {
	private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
			.getLog(DynamicRestClientInterceptor.class);

	protected RestTemplate restTemplate;
	protected ObjectMapper objectMapper;

	private String baseUrl;
	private DynamicRestClientSecurityStrategy securityStrategy;

	public DynamicRestClientInterceptor(String endpointBaseUr, RestTemplate restTemplate, ObjectMapper objectMapper,
			DynamicRestClientSecurityStrategy securityStrategy) {
		this.baseUrl = endpointBaseUr;
		this.restTemplate = restTemplate;
		this.objectMapper = objectMapper;
		this.securityStrategy = securityStrategy;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		RequestMapping requestMappingAnnotation = AnnotationUtils.findAnnotation(invocation.getMethod(),
				RequestMapping.class);
		RestMapping restMappingAnnotation = AnnotationUtils.findAnnotation(invocation.getMethod(), RestMapping.class);
		Parameter[] methodParameters = invocation.getMethod().getParameters();
		Object[] methodArguments = invocation.getArguments();

		// Suggested values that can vary based on method paramters
		HttpMethod sugestedRequestMethod = HttpMethod.GET;
		String suggestedContentType = "application/json";

		if (requestMappingAnnotation == null)
			throw new IllegalStateException(
					"Method " + invocation.getMethod().getName() + " is not annotated as @RequestMapping ");

		// Composes the URL based on @RequestMapping and @RestMapping
		// annotations
		UriComponentsBuilder uriBuilder;
		if (restMappingAnnotation == null)
			uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl).path(requestMappingAnnotation.path()[0]);
		else
			uriBuilder = UriComponentsBuilder.fromHttpUrl(restMappingAnnotation.value())
					.path(requestMappingAnnotation.path()[0]);

		// Build service URL
		Map<String, String> pathVariableExpansions = getPathVariableExpansions(methodParameters, methodArguments);
		MultiValueMap<String, String> requestParamsExpansions = getRequestParamsExpansions(methodParameters,
				methodArguments);
		UriComponents uriComponents = uriBuilder.queryParams(requestParamsExpansions)
				.buildAndExpand(pathVariableExpansions);

		// Process request body
		Object bodyContent = null;
		for (int i = 0; i < methodParameters.length; i++) {
			Parameter parameter = methodParameters[i];

			RequestBody annotation = AnnotationUtils.findAnnotation(parameter, RequestBody.class);
			if (annotation != null) {
				bodyContent = methodArguments[i];
				break;
			}
		}

		// Process @RequestPart as form fields
		Map<String, Object> partParameters = new HashMap<>();
		for (int i = 0; i < methodParameters.length; i++) {
			Parameter parameter = methodParameters[i];

			RequestPart annotation = AnnotationUtils.findAnnotation(parameter, RequestPart.class);
			if (annotation != null) {
				Object value = methodArguments[i];
				String param = annotation.name();
				partParameters.put(param, value);
			}
		}

		// Analyze request parts to find best HttpMethod and Content Type
		if (!partParameters.isEmpty()) {
			if (bodyContent != null) {
				throw new IllegalStateException("Method " + invocation.getMethod().getName()
						+ " combines @RequestBody and @RequestPart annotated arguments. This combination is not supported. ");
			}

			boolean bigObjectFound = false;

			for (String param : partParameters.keySet()) {
				Object value = partParameters.get(param);
				bigObjectFound = bigObjectFound || ((!(value instanceof String) && !(value instanceof Number)));
			}

			if (bigObjectFound)
				suggestedContentType = "application/x-www-form-urlencoded";
			else
				suggestedContentType = "multipart/form-data";

			sugestedRequestMethod = HttpMethod.POST;
		}

		// Headers
		HttpHeaders headers = new HttpHeaders();
		if (requestMappingAnnotation.consumes().length > 0)
			headers.set("content-type", requestMappingAnnotation.consumes()[0]);
		else
			headers.set("content-type", suggestedContentType);

		headers.addAll(getHeaderVariableExpansions(methodParameters, methodArguments));

		// Process request method
		HttpMethod requestMethod;
		if (requestMappingAnnotation.method().length > 0) {
			requestMethod = HttpMethod.resolve(requestMappingAnnotation.method()[0].toString());
		} else {
			requestMethod = sugestedRequestMethod;
		}

		// Process form based body
		if (!partParameters.isEmpty()) {
			String contentType = headers.get("content-type").get(0);
			if (contentType.equalsIgnoreCase("application/x-www-form-urlencoded")) {
				MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
				for (String paramName : partParameters.keySet()) {
					params.add(paramName, partParameters.get(paramName).toString());
				}

				bodyContent = UriComponentsBuilder.newInstance().queryParams(params).build().toUriString().substring(1);
			} else if (contentType.equalsIgnoreCase("multipart/form-data")) {
				throw new IllegalStateException("Method " + invocation.getMethod().getName()
						+ " has @RequestPart annotated arguments and 'multipart/form-data' content type. This combination is not YET supported.");
			} else {
				throw new IllegalStateException(
						"Method " + invocation.getMethod().getName() + " has @RequestPart annotated arguments and '"
								+ contentType + "' content type. This combination is not supported.");
			}
		}

		// Response type
		invocation.getMethod().getGenericReturnType();
		Type returnType = invocation.getMethod().getGenericReturnType();

		// Request
		if (securityStrategy != null)
			securityStrategy.apply(headers, uriComponents);

		HttpEntity<Object> entity = null;
		if (bodyContent != null)
			entity = new HttpEntity<Object>(bodyContent, headers);
		else
			entity = new HttpEntity<Object>(headers);

		// Parses the response
		JavaType constructedType = objectMapper.getTypeFactory().constructFromCanonical(returnType.getTypeName());

		// Perform the request
		log.debug(String.format("[DynamicRestClientInterceptor] Executing %s request to: %s", requestMethod,
				uriComponents.toString()));
		URI requestUrl = uriComponents.encode(Charset.forName("utf-8")).toUri();

		if (log.isTraceEnabled()) {
			log.trace("Request Headers:");
			entity.getHeaders().forEach((name, value) -> log.trace("\t" + name + ": " + value));
		}

		try {
			ResponseEntity<String> response = restTemplate.exchange(requestUrl, requestMethod, entity, String.class);

			String responseBody = response.getBody();
			if (log.isTraceEnabled()) {
				log.trace("Response:");
				response.getHeaders().forEach((name, value) -> log.trace("\t" + name + ": " + value));
				log.trace(responseBody);
			}

			Object deserializedResponse;
			if (responseBody == null) {
				deserializedResponse = null;
			} else if (returnType.equals(String.class)) {
				deserializedResponse = responseBody;
			} else {
				deserializedResponse = objectMapper.readValue(responseBody, constructedType);
			}

			return deserializedResponse;
		} catch (HttpRestException e) {

			tryToGetErrorMessage(e, e.getBody());

			if (log.isDebugEnabled()) {
				log.warn("Error invoking REST Request:  \n" //
						+ "Failed request URL: " + requestUrl + "\n"//
						+ "Failed request headers: " + headers + "\n"//
						+ "Failed request body: \n " + bodyContent + "\n"//
						+ "Failed response status: " + e.getStatus().value() + " - " + e.getStatus().getReasonPhrase()
						+ "\n"//
						+ "Failed response headers: " + e.getHeaders()//
						+ "Failed response body: \n " + e.getBody());
			} else {
				log.warn(String.format("Error invoking REST Request (Enable DEBUG log for more deatils): %s (%s)",
						requestUrl, e.getStatus()));
			}
			throw e;
		} catch (RestClientResponseException e) {

			tryToGetErrorMessage(e);

			if (log.isDebugEnabled()) {
				log.warn("Error invoking REST Request:  \n" //
						+ "Failed request URL: " + requestUrl + "\n"//
						+ "Failed request headers: " + headers + "\n"//
						+ "Failed request body: \n " + bodyContent + "\n"//
						+ "Failed response status: " + e.getRawStatusCode() + "\n"//
						+ "Failed response headers: " + e.getResponseHeaders()//
						+ "Failed response body: \n " + e.getResponseBodyAsString());
			} else {
				log.warn(String.format("HTTP Error invoking REST Request (Enable DEBUG log for more deatils): %s (%s)",
						requestUrl, e.getRawStatusCode()));
			}
			throw e;
		} catch (Exception e) {
			log.warn("Unexpected error invoking REST Request:  " + requestUrl + " : " + e.getMessage());
			throw e;
		}

	}

	private void tryToGetErrorMessage(RestClientResponseException e) {
		if (e.getResponseBodyAsByteArray() != null) {
			tryToGetErrorMessage(e, e.getResponseBodyAsString());
		}
	}

	private void tryToGetErrorMessage(Throwable e, String body) {
		try {

			ObjectNode response = objectMapper.readValue(body, ObjectNode.class);

			JsonNode messageNode = response.get("message");
			if (messageNode != null) {
				String message = messageNode.asText();

				Field field = ReflectionUtils.findField(Throwable.class, "detailMessage");
				ReflectionUtils.makeAccessible(field);
				ReflectionUtils.setField(field, e, message);
			}
		} catch (Exception ex) {
			if (log.isDebugEnabled())
				log.warn("Cant't extract exception message from server response.", ex);
		}
	}

	protected MultiValueMap<String, String> getRequestParamsExpansions(Parameter[] methodParameters,
			Object[] methodArguments) throws IllegalStateException {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		for (int i = 0; i < methodParameters.length; i++) {
			Parameter parameter = methodParameters[i];
			Object argument = methodArguments[i];

			RequestParam annotation = AnnotationUtils.findAnnotation(parameter, RequestParam.class);
			if (annotation != null && argument != null) {
				if (!annotation.name().isEmpty()) {
					params.add(annotation.name(), encode(argument.toString()));
				} else {
					throw new IllegalStateException("Argument " + i + " of method has no valid @RequestParam name");
				}
			}
		}
		return params;
	}

	protected Map<String, String> getPathVariableExpansions(Parameter[] methodParameters, Object[] methodArguments)
			throws IllegalStateException {
		Map<String, String> expandValues = new HashMap<>();
		for (int i = 0; i < methodParameters.length; i++) {
			Parameter parameter = methodParameters[i];

			PathVariable annotation = AnnotationUtils.findAnnotation(parameter, PathVariable.class);
			if (annotation != null) {
				String key;
				if (!annotation.value().isEmpty()) {
					key = annotation.name();
				} else {
					throw new IllegalStateException("Argument " + i + " of method has no valid @PathVariable name");
				}

				Object argument = methodArguments[i];
				if (argument instanceof Collection) {
					expandValues.put(key,
							encode(((Collection<?>) argument).stream().map(v -> v.toString()).collect(joining(","))));
				} else {
					expandValues.put(key, encode(methodArguments[i].toString()));
				}
			}
		}
		return expandValues;
	}

	protected MultiValueMap<String, String> getHeaderVariableExpansions(Parameter[] methodParameters,
			Object[] methodArguments) throws IllegalStateException {

		MultiValueMap<String, String> expandValues = new LinkedMultiValueMap<>();

		for (int i = 0; i < methodParameters.length; i++) {
			Parameter parameter = methodParameters[i];

			RequestHeader annotation = AnnotationUtils.findAnnotation(parameter, RequestHeader.class);
			if (annotation != null) {
				String key;
				if (!annotation.value().isEmpty()) {
					key = annotation.name();
				} else {
					throw new IllegalStateException("Argument " + i + " of method has no valid @PathVariable name");
				}

				Object argument = methodArguments[i];
				if (argument instanceof Collection) {

					expandValues.put(key, //
							((Collection<?>) argument).stream()//
									.map(v -> encode(v.toString())).collect(Collectors.toList())//
					);
				} else {
					expandValues.add(key, encode(argument.toString()));
				}
			}
		}
		return expandValues;

	}

	private String encode(String text) {
		return text;
		// try {
		// return URLEncoder.encode(text, "utf-8");
		// } catch (UnsupportedEncodingException e) {
		// throw new RuntimeException(e);
		// }
	}
}
