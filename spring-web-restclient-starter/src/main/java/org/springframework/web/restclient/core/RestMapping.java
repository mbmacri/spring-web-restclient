package org.springframework.web.restclient.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface RestMapping {

	/**
	 * The endpoint base URL for the service.
	 */
	String value() default "";
	
	/**
	 * Bean name
	 */
	String bean() default "";
}
