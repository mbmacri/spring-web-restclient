package org.springframework.web.restclient.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

public class InternalServerErrorHttpRestException extends HttpRestException {
	private static final long serialVersionUID = -5686587637134399758L;

	public InternalServerErrorHttpRestException() {
		super();
	}

	public InternalServerErrorHttpRestException(ClientHttpResponse response) {
		super(response);
	}

}
