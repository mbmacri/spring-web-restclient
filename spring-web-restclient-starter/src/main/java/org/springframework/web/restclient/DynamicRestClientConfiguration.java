package org.springframework.web.restclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.restclient.core.DynamicRestClientFactory;
import org.springframework.web.restclient.core.DynamicRestClientSecurityStrategy;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableConfigurationProperties(DynamicRestClientProperties.class)
public class DynamicRestClientConfiguration {
	@Autowired
	private DynamicRestClientProperties properties;

	@Bean("dynamicRestClientFactory")
	public DynamicRestClientFactory dynamicRestClientFactory(RestTemplate restTemplate, ObjectMapper objectMapper,
			@Autowired(required = false) DynamicRestClientSecurityStrategy securityStrategy) {

		String actualDefaultEndpoint = DynamicRestClientBeansRegistrar.defaultEndpoint != null
				? DynamicRestClientBeansRegistrar.defaultEndpoint
				: properties.getDefaultEndpoint();

		DynamicRestClientFactory dynamicRestClientFactory = new DynamicRestClientFactory(restTemplate, objectMapper,
				securityStrategy, actualDefaultEndpoint);
		dynamicRestClientFactory.setEndpoints(properties.getEndpoint());

		return dynamicRestClientFactory;
	}

}
