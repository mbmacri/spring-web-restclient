package org.springframework.web.restclient.tests;

import java.util.List;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.restclient.core.RestMapping;

@RestMapping
public interface TestService {

	@RequestMapping(path="/my/path", method=RequestMethod.GET)
	public void headerTest(@RequestHeader(name="myHeader") String arg);
	
	@RequestMapping(path="/my/path/list", method=RequestMethod.GET)
	public void headerTest(@RequestHeader(name="myHeader") List<String> arg);
	
}
