package org.springframework.web.restclient;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import({ DynamicRestClientBeansRegistrar.class, DynamicRestClientConfiguration.class })
public @interface EnableRestClients {

	String[] classPaths() default {};

	String defaultEndPoint() default "";
}
