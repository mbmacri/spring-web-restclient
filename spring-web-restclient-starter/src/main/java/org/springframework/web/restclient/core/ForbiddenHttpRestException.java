package org.springframework.web.restclient.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

public class ForbiddenHttpRestException extends HttpRestException {
	private static final long serialVersionUID = -5686587637134399759L;

	public ForbiddenHttpRestException() {
		super();
	}

	public ForbiddenHttpRestException(ClientHttpResponse response) {
		super(response);
	}

}
