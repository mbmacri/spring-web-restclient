package org.springframework.web.restclient.tests;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

public class TestingExceptionManipulation {

	@Test
	public void test() {
		HttpStatusCodeException e = new HttpClientErrorException(HttpStatus.NOT_FOUND);

		System.out.println(e);

		Field field = ReflectionUtils.findField(Throwable.class, "detailMessage");
		ReflectionUtils.makeAccessible(field);
		ReflectionUtils.setField(field, e, "Mi mensaje hackeado");
		
		System.out.println(e);
	}

}
