package org.springframework.web.restclient.core;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

public class HttpRestException extends RuntimeException {
	private static final long serialVersionUID = -2886258808648609637L;
	private HttpStatus status;
	private String headers;
	private String body;

	public HttpRestException() {
	}

	@Override
	public String toString() {
		return String.format("HTTP Rest Exception: %s - %s", status.value(), status.getReasonPhrase());
	}

	public HttpRestException(ClientHttpResponse response) {
		try {
			this.status = response.getStatusCode();
			this.headers = response.getHeaders().toString();
			this.body = IOUtils.toString(response.getBody(), Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public static HttpRestException forFailedResponse(ClientHttpResponse response) {
		try {
			switch (response.getStatusCode()) {
			case OK:
				return null;
			case NOT_FOUND:
				return new NotFoundHttpRestException(response);
			case BAD_REQUEST:
				return new BadRequestHttpRestException(response);
			case INTERNAL_SERVER_ERROR:
				return new InternalServerErrorHttpRestException(response);
			case FORBIDDEN:
				return new ForbiddenHttpRestException(response);
			default:
				return new HttpRestException(response);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
