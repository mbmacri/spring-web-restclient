package org.springframework.web.restclient;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "rest.client")
public class DynamicRestClientProperties {

	private String contextHeader = null;
	private String defaultEndpoint = null;
	private Map<String, String> endpoint = new HashMap<>();

	public String getContextHeader() {
		return contextHeader;
	}

	public void setContextHeader(String contextHeader) {
		this.contextHeader = contextHeader;
	}

	public String getDefaultEndpoint() {
		return defaultEndpoint;
	}

	public void setDefaultEndpoint(String defaultEndpoint) {
		this.defaultEndpoint = defaultEndpoint;
	}

	public Map<String, String> getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Map<String, String> endpoint) {
		this.endpoint = endpoint;
	}
}
