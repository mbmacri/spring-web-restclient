package org.springframework.web.restclient.core;

import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponents;

public interface DynamicRestClientSecurityStrategy {
	public void apply(HttpHeaders entity, UriComponents uriBuilder);
}
