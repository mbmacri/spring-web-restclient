package org.springframework.web.restclient;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.CannotLoadBeanClassException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.restclient.core.RestMapping;
import org.springframework.web.restclient.utils.InterfacesClassPathScanningCandidateComponentProvider;

@Configuration
public class DynamicRestClientBeansRegistrar implements ImportBeanDefinitionRegistrar, BeanClassLoaderAware {

	private static final Log log = LogFactory.getLog(DynamicRestClientBeansRegistrar.class);

	static String[] scanClassPaths = null;
	static String defaultEndpoint = null;

	public void setImportMetadata(AnnotationMetadata importMetadata) {

		Class<EnableRestClients> annoType = EnableRestClients.class;
		Map<String, Object> annotationAttributes = importMetadata.getAnnotationAttributes(annoType.getName(), false);
		AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationAttributes);

		scanClassPaths = attributes.getStringArray("classPaths");

		if (scanClassPaths == null || scanClassPaths.length == 0) {
			String[] split = importMetadata.getClassName().split("\\.");
			split = Arrays.copyOfRange(split, 0, split.length - 1);
			scanClassPaths = new String[] { String.join(".", split) };
		}

		defaultEndpoint = attributes.getString("defaultEndPoint");
		if (defaultEndpoint.isEmpty())
			defaultEndpoint = null;
	}

	@Override
	public void setBeanClassLoader(ClassLoader classLoader) {
	}

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

		setImportMetadata(importingClassMetadata);

		ClassPathScanningCandidateComponentProvider provider = new InterfacesClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new AnnotationTypeFilter(RestMapping.class, false, true));

		Set<BeanDefinition> candidates = new HashSet<>();

		for (String pkg : scanClassPaths) {
			candidates.addAll(provider.findCandidateComponents(pkg));
		}

		for (BeanDefinition beanDefinition : candidates) {
			String beanClassName = beanDefinition.getBeanClassName();
			Class<?> clazz;
			try {
				clazz = Class.forName(beanClassName);
			} catch (ClassNotFoundException e) {
				throw new CannotLoadBeanClassException("DynamicRestClient", "N/A", beanClassName, e);
			}
			RestMapping restMapping = clazz.getAnnotation(RestMapping.class);

			String beanName = StringUtils.isEmpty(restMapping.bean()) ? ClassUtils.getShortNameAsProperty(clazz) : restMapping.bean();

			GenericBeanDefinition proxyBeanDefinition = new GenericBeanDefinition();
			proxyBeanDefinition.setBeanClass(clazz);

			ConstructorArgumentValues args = new ConstructorArgumentValues();
			args.addGenericArgumentValue(clazz);
			proxyBeanDefinition.setConstructorArgumentValues(args);

			proxyBeanDefinition.setFactoryBeanName("dynamicRestClientFactory");
			proxyBeanDefinition.setFactoryMethodName("createRestClient");

			registry.registerBeanDefinition(beanName, proxyBeanDefinition);
			log.debug(String.format("DynamicRestClient bean registed for class: %s", beanClassName));
		}
	}
}
