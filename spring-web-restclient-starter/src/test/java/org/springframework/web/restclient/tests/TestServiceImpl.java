package org.springframework.web.restclient.tests;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestServiceImpl {

	private static final Logger log = LoggerFactory.getLogger(TestServiceImpl.class);

	String receivedArg = null;
	List<String> receivedList = null;

	@RequestMapping(path = "/my/path", method = RequestMethod.GET)
	public void headerTest(@RequestHeader(name = "myHeader") String arg) {
		receivedArg = arg;

		log.info("[/my/path] arg=" + arg);
	}
	
	@RequestMapping(path = "/my/path/list", method = RequestMethod.GET)
	public void headerTestLest(@RequestHeader(name = "myHeader") List<String> arg) {
		receivedList = arg;
		
		log.info("[/my/path/list] arg=" + arg);
	}

}
