package org.springframework.web.restclient.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

public class NotFoundHttpRestException extends HttpRestException {
	private static final long serialVersionUID = -5686587637134399750L;

	public NotFoundHttpRestException() {
	}

	public NotFoundHttpRestException(ClientHttpResponse response) {
		super(response);
	}

}
