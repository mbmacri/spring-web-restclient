package org.springframework.web.restclient.core;

import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponents;

public class DynamicRestClientJwtSecurityStrategy implements DynamicRestClientSecurityStrategy {
	private String jwtToken;

	public DynamicRestClientJwtSecurityStrategy() {
		this(null);
	}

	public DynamicRestClientJwtSecurityStrategy(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	@Override
	public void apply(HttpHeaders headers, UriComponents uriBuilder) {
		if (jwtToken != null) {
			headers.add("Authorization", "Bearer " + jwtToken);
		}
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

}
