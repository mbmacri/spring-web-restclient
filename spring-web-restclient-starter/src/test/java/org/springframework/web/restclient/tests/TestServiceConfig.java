package org.springframework.web.restclient.tests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.restclient.EnableRestClients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;

@SpringBootApplication
@EnableRestClients(defaultEndPoint = "http://localhost:8088/test", classPaths = "org.springframework.web.restclient.tests")
public class TestServiceConfig {

	public static void main(String[] args) {
		SpringApplication.run(TestServiceConfig.class, args);
	}

	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		return mapper;
	}

	@Bean
	public RestTemplate restTemplate(ObjectMapper mapper) {
		RestTemplate restTemplate = new RestTemplateBuilder().build();

		restTemplate.getMessageConverters().replaceAll(mc -> {
			if (mc instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
				jsonMessageConverter.setObjectMapper(mapper);
				return jsonMessageConverter;
			} else {
				return mc;
			}
		});

		return restTemplate;
	}
}
