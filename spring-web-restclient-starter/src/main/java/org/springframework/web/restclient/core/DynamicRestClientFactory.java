package org.springframework.web.restclient.core;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DynamicRestClientFactory {

	private static final Logger log = LoggerFactory.getLogger(DynamicRestClientFactory.class);

	protected RestTemplate restTemplate;
	protected ObjectMapper objectMapper;
	private DynamicRestClientSecurityStrategy securityStrategy;
	private String defaultEndpoint;
	private Map<String, String> endpoints = new HashMap<>();

	public DynamicRestClientFactory(RestTemplate restTemplate, ObjectMapper objectMapper,
			DynamicRestClientSecurityStrategy securityStrategy, String defaultEndpoint) {
		super();
		this.restTemplate = restTemplate;
		this.objectMapper = objectMapper;
		this.securityStrategy = securityStrategy;
		this.defaultEndpoint = defaultEndpoint;
	}

	public Map<String, String> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(Map<String, String> endpoints) {
		this.endpoints = endpoints;
	}

	public <C> C createRestClient(Class<C> restServiceInterface) {
		String endpointUrl = lookForAnnotatedRootUrl(restServiceInterface);

		if (endpointUrl == null || endpointUrl.isEmpty())
			throw new BeanCreationException(
					"No default endpoint definded. Use annotated configuration or set 'rest.client.defaultEndpoint' property.");

		return createRestClient(restServiceInterface, endpointUrl);
	}

	private <C> C createRestClient(Class<C> restServiceInterface, String endpointBaseUrl) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(endpointBaseUrl);
		String relativeUrl = lookForRequestMappingRelativeUrl(restServiceInterface);
		if (relativeUrl != null)
			uriBuilder.path(relativeUrl);

		C proxy = ProxyFactory.getProxy(restServiceInterface, new DynamicRestClientInterceptor(
				uriBuilder.build().toString(), restTemplate, objectMapper, securityStrategy));
		
		log.info("REST Client: New DynamicRestClient created for interface " + restServiceInterface.getName() + " [" + endpointBaseUrl + "]");
		
		return proxy;
	}

	private String lookForRequestMappingRelativeUrl(Class<?> restServiceInterface) {
		RequestMapping annotation = AnnotationUtils.findAnnotation(restServiceInterface, RequestMapping.class);

		if (annotation == null)
			return "";

		String[] paths = annotation.path();
		if (paths.length > 0)
			return paths[0];
		else
			return "";
	}

	private String lookForAnnotatedRootUrl(Class<?> restServiceInterface) {
		RestMapping annotation = AnnotationUtils.findAnnotation(restServiceInterface, RestMapping.class);

		if (annotation == null)
			throw new IllegalArgumentException("Class " + restServiceInterface
					+ " is not annotated with @RestMapping annotation. Use the contructor with explicit base URL.");

		if (endpoints.containsKey(restServiceInterface.getName()))
			return endpoints.get(restServiceInterface.getName());
		else if (annotation.value().isEmpty())
			return defaultEndpoint;
		else
			return annotation.value();
	}

}
